package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Article {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false, unique = true)
	@NotNull
	@Size(min=2,max=50)
	@NotEmpty(message = "Le nom est obligatoire")
	private String name;
	
	@NotNull
	@Size(min=2,max=50)
	@Column(nullable = false)
	private String author;
	
	@NotNull
	@Size(min=2,max=1000)
	@Column(nullable = false, columnDefinition="Text")
	private String description;
	
	@NotNull
	@Size(min=2,max=50)
	@Column(nullable = false)
	private String editor;
	
	@NotNull
	@Min(2)
	@Column(nullable = false)
	private int page;
	
	@NotNull
	@Min(18)
	@Column(nullable = false)
	private Float price;
	
	public Article() {
		
	}

	public Article(String name, String author, String description, String editor, int page, Float price) {
		this.name = name;
		this.author = author;
		this.description = description;
		this.editor = editor;
		this.page = page;
		this.price = price;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", name=" + name + ", author=" + author + ", description=" + description
				+ ", editor=" + editor + ", page=" + page + ", price=" + price + "]";
	}
	
	
}