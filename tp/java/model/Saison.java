package model;

public class Saison {
	
	private int id;
	private String nom;
	private String pathImg;
	private boolean current;
	
	public Saison() {
		
	}
	
	public Saison(String nom, String pathImg, boolean current) {
		super();
		this.nom = nom;
		this.pathImg = pathImg;
		this.current = current;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPathImg() {
		return pathImg;
	}

	public void setPathImg(String pathImg) {
		this.pathImg = pathImg;
	}

	public boolean isCurrent() {
		return current;
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}

	@Override
	public String toString() {
		return "Saison [id=" + id + ", nom=" + nom + ", pathImg=" + pathImg + ", current=" + current + "]";
	}
	
	
}
