package fr.ajc.spring.perroux.tp;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import model.Article;
import repo.ArticleRepository;

@Controller
@RequestMapping("/articles")
public class ArticleControllerC {
	@Autowired
	private ArticleRepository articleRepository;
	
	@GetMapping
	public String listeArticle(Model model) {
		List<Article> articles = (List<Article>) articleRepository.findAll();
		model.addAttribute("articles",articles);
		return "listeArticle";
	}
	
	@GetMapping("searching")
	public String search(@RequestParam String searching,Model model) {
		System.out.println("search= "+searching);
		List<Article> articles = (List<Article>) articleRepository.findByNameIgnoreCaseContainingOrAuthorIgnoreCaseContainingOrEditorIgnoreCaseContainingOrDescriptionIgnoreCaseContaining(searching,searching,searching,searching);
		model.addAttribute("articles",articles);
		return "listeArticle";
	}

	@GetMapping("article/{name}")
	public String findByName(@PathVariable String name, Model model) {
		List<Article> articles = articleRepository.findByName(name);
		model.addAttribute("article",articles.get(0));
		return "show";
	}
	
	@GetMapping("create")
	public String create(Model model, Article article) {
		return "createArticle";
	}
	
	@GetMapping("search")
	public String search(Model model) {
		return "search";
	}
	
	@PostMapping
	public String create(Model model, @Valid Article article, BindingResult result) {
		if(result.hasErrors()) {
			return "createArticle";
		}
		
		articleRepository.save(article);
		List<Article> articles = (List<Article>) articleRepository.findAll();
		model.addAttribute("articles",articles);
		return "redirect:/articles";
	}
	
	@GetMapping("delete/{id}")
	public String delete(@PathVariable Long id,Model model) {
		articleRepository.deleteById(id);
		List<Article> articles = (List<Article>) articleRepository.findAll();
		model.addAttribute("articles",articles);
		return "redirect:/articles";
	}
}