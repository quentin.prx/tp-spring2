package fr.ajc.spring.perroux.tp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import model.Article;
import model.Saison;
import repo.ArticleRepository;

@Controller
public class SimpleController {
	@Value("${spring.application.name}")
	String appName;
	
	@GetMapping("/")
	public String homePage(Model model) {
		model.addAttribute("appName", appName);
		return "home";
	}
	
	/*@GetMapping("create")
	public String create(Model model) {
		return "createArticle";
	}*/
	
	@GetMapping("date")
	public String datePage(Model model) throws ParseException {
		LocalDateTime localDate = LocalDateTime.now();
		
		String boulot = "";
		switch(localDate.getDayOfWeek().getValue()) {
			case 1:
				boulot = "oui";
				break;
			case 2:
				boulot = "non";
				break;
			case 3:
				boulot = "oui";
				break;
			case 4:
				boulot = "oui";
				break;
			case 5:
				boulot = "oui";
				break;
			case 6:
				boulot = "non";
				break;
			case 7:
				boulot = "non";
				break;
			default:
				boulot = "oui";
		}
		
		String saison = "";
		List<Saison> saisons = new ArrayList<Saison>();
		Saison h = new Saison("Hiver", "https://chapelleriedesarcades.fr/wp-content/uploads/2018/07/Photo-hiver.jpg", false);
		Saison p = new Saison("Printemps","https://static.cybercartes.com/ccimg/images_cc/diapos_300x180/cc_el_130071.jpg",false);
		Saison e = new Saison("Eté","https://asso-ideal.fr/wp-content/uploads/2018/06/Summer-in-the-sand.jpg",false);
		Saison a = new Saison("Automne","https://i.ytimg.com/vi/FQKmWsD1Mrw/hqdefault.jpg",false);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date now = new Date();
        Date hiverD = sdf.parse("2018-12-21");
        Date printempsD = sdf.parse("2019-03-21");
        Date eteD = sdf.parse("2019-06-21");
        Date automneD = sdf.parse("2019-09-21");
        
        if(now.after(hiverD) && now.before(printempsD)) {
        	h.setCurrent(true);
        	saison = "hiver";
        }else if(now.after(printempsD) && now.before(eteD)) {
        	p.setCurrent(true);
        	saison = "printemps";
        } else if (now.after(eteD) && now.before(automneD)) {
        	e.setCurrent(true);
        	saison = "ete";
        }else if (now.after(automneD) && now.before(hiverD)) {
        	a.setCurrent(true);
        	saison = "automne";
        }else {
        	saison = "ete";
        	e.setCurrent(true);
        }
        
        saisons.add(h);
        saisons.add(p);
        saisons.add(e);
        saisons.add(a);
		
        model.addAttribute("saisons",saisons);
        model.addAttribute("saison",saison);
		model.addAttribute("boulot",boulot);
		model.addAttribute("localDateTime", localDate);
		return "date";
	}
}