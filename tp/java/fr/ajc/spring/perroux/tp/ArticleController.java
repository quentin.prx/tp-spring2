package fr.ajc.spring.perroux.tp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import model.Article;
import repo.ArticleRepository;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {
	@Autowired
	private ArticleRepository articleRepository;

	@GetMapping
	public Iterable<Article> findAll() {
		List<Article> articles = (List<Article>) articleRepository.findAll();
		
		return articles;
	}

	@GetMapping("/article/{name}")
	public List<Article> findByName(@PathVariable String name) {
		System.out.println(articleRepository.findByName(name));
		List<Article> articles = articleRepository.findByName(name);
		return articles;
	}
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Article create(Article article) {
		return articleRepository.save(article);
	}
	
	@DeleteMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public String delete(Long id) {
		articleRepository.deleteById(id);
		return "Article supprimé";
	}
}