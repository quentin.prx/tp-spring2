package fr.ajc.spring.perroux.tp;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

import model.Article;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TpApplicationTests {
	public static final String API_ROOT = "http://localhost:8081/api/articles";

	@Test
	public void contextLoads() {
	}

	@Test
	public void whenGetAllArticles_thenOK() {
		Response response = RestAssured.get(API_ROOT);
		System.out.println(response.body().asString());
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
	}

	@Test
	public void whenGetArticleByName_thenOK() {
		Article article = createRandomArticle();
		createArticleAsUri(article);
		Response response = RestAssured.get(
				API_ROOT + "/" + article.getName());
		System.out.println("name "+article.getName());
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
	}


	private String createArticleAsUri(Article article) {
		Response response =
				RestAssured.given().contentType(MediaType.APPLICATION_JSON_VALUE).body(article).post(API_ROOT);
		return API_ROOT + "/" + response.jsonPath().get("id");
	}
	
	private Article createRandomArticle() {
		float min = 0.0F;
		float max = 2000.0F;
		List<String> articlesNames = new ArrayList<String>(){{
			add("Livre");
			add("Voiture");
			add("Ordinateur");
			add("Téléphone");
			add("Trousse");
		
		}};
		int minS = 0;
		int maxS = articlesNames.size()-1;
		System.out.println("Avant indexAl - size "+articlesNames.size());
		int indexAl = new Random().nextInt(maxS-minS);
		//int indexAl = 4;
		System.out.println("Après indexAl "+indexAl);
		String name = articlesNames.get(indexAl);
		float price  = min + new Random().nextFloat()*(max-min);
		System.out.println("Price : "+price);
		Article a = new Article(name,name,name,name,1000, price);
		return a;
	}
}
